import React, {Component} from "react";
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';

class App extends Component {
  state = {
    firstNumber:0,
    secondNumber:0,
    Addidition:0,
    subtraction:0,
    division:0,
    multiplication:0,
  }
  handleFisrtNumber = (text) => {
    this.setState({ firstNumber:text })
  }
  handleSecondNumber = (text) => {
    this.setState({ secondNumber: text })
  }
  computeOperation = (firstNumber, secondNumber) => {
    this.setState({
             Addidition: firstNumber + secondNumber,
             subtraction: firstNumber - secondNumber,
             division: firstNumber / secondNumber,
             multiplication: firstNumber*secondNumber,
              })
  }
  render() {
    return (
          <View style = {styles.container}>
               <TextInput style = {styles.input}
                   underlineColorAndroid = "transparent"
                   placeholder = "First Number"
                   placeholderTextColor = "#9a73ef"
                   autoCapitalize = "none"
                   onChangeText = {this.handleFisrtNumber}/>
                
                <TextInput style = {styles.input}
                   underlineColorAndroid = "transparent"
                   placeholder = "First Number"
                   placeholderTextColor = "#9a73ef"
                   autoCapitalize = "none"
                   onChangeText = {this.handleSecondNumber}/>
                
                <TouchableOpacity
                   style = {styles.submitButton}
                   onPress = {
                      () => this.computeOperation(this.state.firstNumber, this.state.secondNumber)
                   }>
                   <Text style = {styles.submitButtonText}> Submit </Text>
                </TouchableOpacity>

        <Text>Addition: {this.state.Addidition}</Text>
        <Text>subtraction: {this.state.subtraction}</Text>
        <Text>division: {this.state.division}</Text>
        <Text>multiplication: {this.state.multiplication}</Text>
        <StatusBar style="auto" />
      </View>
    )
  }
}
export default App

const styles = StyleSheet.create({
   container: {
      paddingTop: 23
   },
   input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1
   },
   submitButton: {
      backgroundColor: '#7a42f4',
      padding: 10,
      margin: 15,
      height: 40,
   },
   submitButtonText:{
      color: 'white'
   }
})
